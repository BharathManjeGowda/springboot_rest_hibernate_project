How to run project:-

1. Download the project.
2. Open in IntelIj.
3. Maven clean install.
4. Run SpringBootHelloWorldApplication.java as Java Application.

_____________________________________________________________________________

REST end points:-

1. Save project: POST : http://localhost:8080/project

BODY: {
  "projectDetails": {
    "name": "b",
    "description": "d",
    "summary": "f"
  },
  "dateRequested": "2012-01-30T00:00:00",
  "dateRequired": "2012-01-30T00:00:00",
  "keyContacts": {
    "firstName": "b",
    "lastName": "d",
    "phone": 2134234,
    "email": "d",
    "role": "d",
    "team": "d"
  },
  "estimates": 2,
  "critical": true
}
_____________________________________________________________________________

2. Get Project by ID - GET - http://localhost:8080/project/1

_____________________________________________________________________________

3. Get all Projects - GET - http://localhost:8080/projects

_____________________________________________________________________________

4. Update a Project - PUT - http://localhost:8080/project/1

BODY: {
  "projectDetails": {
    "name": "b",
    "description": "d",
    "summary": "f"
  },
  "dateRequested": "2012-01-30T00:00:00",
  "dateRequired": "2012-01-30T00:00:00",
  "keyContacts": {
    "firstName": "b",
    "lastName": "d",
    "phone": 2134234,
    "email": "d",
    "role": "d",
    "team": "d"
  },
  "estimates": 2,
  "critical": true
}
_____________________________________________________________________________

5. Delete a Project - DELETE - http://localhost:8080/project/1

_____________________________________________________________________________



