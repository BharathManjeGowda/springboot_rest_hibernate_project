package com.javainuse.controller;

import com.javainuse.exception.ProjectCustomException;
import com.javainuse.model.KeyContacts;
import com.javainuse.model.Project;
import com.javainuse.model.ProjectDetails;
import com.javainuse.service.ProjectService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.mock;

public class ProjectControllerTest {

	@InjectMocks
	private ProjectController projectController;

	@Mock
	private ProjectService projectService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	HttpServletResponse response = mock(HttpServletResponse.class);

	@Test
	public void testSaveProject() {
		Mockito.when(projectService.saveProject(Mockito.anyObject())).thenReturn(getProject().getProjectId());
		Long id = projectController.newProject(getProject(), response);
		Assert.assertEquals(id ,Long.valueOf(1l));
	}

	@Test
	public void testGetProjectById() {
		Mockito.when(projectService.findProjectById(Mockito.anyLong())).thenReturn(getProject());
		Project project = projectController.getProjectById(1l);
		Assert.assertEquals(project.getProjectId(),Long.valueOf(1l));
	}

	@Test
	public void testGetProjects() {
		List<Project> projects = new ArrayList<>();
		projects.add(getProject());
		Mockito.when(projectService.getProjectsList()).thenReturn(projects);
		List<Project> projectsList = projectController.getProjectsList();
		Assert.assertEquals(projectsList.size(), 1);
	}

	@Test
	public void testUpdateProject() throws ProjectCustomException {
		Mockito.when(projectService.updateProject(Mockito.anyLong(), Mockito.anyObject())).thenReturn(getProject());
		Project project = projectController.updateProject(1l, getProject(), response);
		Assert.assertEquals(project.getProjectId() ,Long.valueOf(1l));
	}

	public Project getProject(){
		Project project = new Project();
		project.setProjectId(1l);

		ProjectDetails projectDetails = new ProjectDetails();
		projectDetails.setDescription("Test Desc");
		projectDetails.setName("Test Name");
		project.setProjectDetails(projectDetails);

		KeyContacts keyContacts = new KeyContacts();
		keyContacts.setEmail("test@test.com");
		project.setKeyContacts(keyContacts);

		project.setDateRequested(new Date());
		project.setDateRequired(new Date());
		project.setCritical(true);
		return project;
	}

}
