package com.javainuse.service;

import com.javainuse.controller.ProjectControllerTest;
import com.javainuse.dao.ProjectRepository;
import com.javainuse.model.KeyContacts;
import com.javainuse.model.Project;
import com.javainuse.model.ProjectDetails;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProjectServiceTest {

	@InjectMocks
	private ProjectService projectService;

	@Mock
	private ProjectRepository projectData;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSaveProject() {
		//Mockito.when(projectData.save(Mockito.anyObject())).thenReturn(getProject());
		Long id = projectService.saveProject(getProject());
		Assert.assertEquals(id ,Long.valueOf(1l));
	}

	@Test
	public void testFindProjectById() {
		Mockito.when(projectData.findByProjectId(Mockito.anyLong())).thenReturn(getProject());
		List<Project> projectsList = projectService.getProjectsList();
		Assert.assertEquals(projectsList.size() , 1);
	}

	@Test
	public void testGetProjects() {
		List<Project> projects = new ArrayList<>();
		projects.add(getProject());
		Mockito.when(projectData.findAll()).thenReturn(projects);
		List<Project> projectsList = projectService.getProjectsList();
		Assert.assertEquals(projectsList.size() , 1);
	}

	private Project getProject(){
		ProjectControllerTest projectControllerTest = new ProjectControllerTest();
		Project project = projectControllerTest.getProject();
		return project;
	}

}
