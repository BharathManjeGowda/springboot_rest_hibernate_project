package com.javainuse.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Project {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private	Long projectId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PROJECT_DETAILS_ID")
	private	ProjectDetails projectDetails;

	private	Date dateRequested;

	private	Date dateRequired;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "KEY_CONTACTS_ID")
	private	KeyContacts keyContacts;

	private	int estimates;

	private	ProjectType projectType;

	private	Boolean isCritical;

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public ProjectDetails getProjectDetails() {
		return projectDetails;
	}

	public void setProjectDetails(ProjectDetails projectDetails) {
		this.projectDetails = projectDetails;
	}

	public Date getDateRequested() {
		return dateRequested;
	}

	public void setDateRequested(Date dateRequested) {
		this.dateRequested = dateRequested;
	}

	public Date getDateRequired() {
		return dateRequired;
	}

	public void setDateRequired(Date dateRequired) {
		this.dateRequired = dateRequired;
	}

	public KeyContacts getKeyContacts() {
		return keyContacts;
	}

	public void setKeyContacts(KeyContacts keyContacts) {
		this.keyContacts = keyContacts;
	}

	public int getEstimates() {
		return estimates;
	}

	public void setEstimates(int estimates) {
		this.estimates = estimates;
	}

	public ProjectType getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = ProjectType.valueOf(projectType);
	}

	public Boolean getCritical() {
		return isCritical;
	}

	public void setCritical(Boolean critical) {
		isCritical = critical;
	}
}
