package com.javainuse.model;

import javax.persistence.*;

@Entity
@Table(name = "PROJECT_DETAILS")
public class ProjectDetails {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PROJECT_DETAILS_ID")
	private int projectDetailsId;
	private	String name;
	private	String description;
	private	String summary;

    public int getProjectDetailsId() {
        return projectDetailsId;
    }

    public void setProjectDetailsId(int projectDetailsId) {
        this.projectDetailsId = projectDetailsId;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}
}
