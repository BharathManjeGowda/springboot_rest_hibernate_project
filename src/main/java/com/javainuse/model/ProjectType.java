package com.javainuse.model;

public enum ProjectType {

    DOCS_MANAGEMENT,
    SECURITIES_AND_COLLATERAL;

}
