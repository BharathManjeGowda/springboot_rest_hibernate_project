package com.javainuse.model;

import javax.persistence.*;

@Entity
@Table(name = "KEY_CONTACTS")
public class KeyContacts {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "KEY_CONTACTS_ID")
	private int keyContactsId;

	private	String firstName;
	private	String lastName;
	private	int phone;
	private	String email;
	private	String role;
	private	String team;

	public int getKeyContactsId() {
		return keyContactsId;
	}

	public void setKeyContactsid(int keyContactsId) {
		this.keyContactsId = keyContactsId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}
}
