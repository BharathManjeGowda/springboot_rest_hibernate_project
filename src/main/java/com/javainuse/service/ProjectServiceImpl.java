package com.javainuse.service;

import com.javainuse.dao.ProjectRepository;
import com.javainuse.exception.ProjectCustomException;
import com.javainuse.model.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectService.class);

    @Autowired
    private ProjectRepository projectData;

    @Override
    @Transactional
    public Long saveProject(Project project) {
        Project newProject = new Project();
        try {
            newProject = projectData.save(project);
        } catch (Exception ex){
            LOG.debug("Error while creating new project" + ex);
        }

        return newProject.getProjectId();
    }

    @Override
    public List<Project> getProjectsList() {
        List<Project> allProjects = projectData.findAll();
        LOG.debug("Total number fo projects" + allProjects.size());
        return allProjects;
    }

    @Override
    public Project findProjectById(Long id) {
        Project project = projectData.findByProjectId(id);
        return project;
    }

    @Override
    @Transactional
    public void deleteProjectById(Long id) throws ProjectCustomException {
        try {
            projectData.delete(id);
        }
        catch (EmptyResultDataAccessException ex){
            LOG.debug("No project was found for the id" + id);
            throw new ProjectCustomException("No project was found for the id" + id);
        }
        catch (Exception ex){
            LOG.debug("Error while deleting a project" + ex);
        }
    }

    @Override
    @Transactional
    public Project updateProject(Long id, Project project) throws ProjectCustomException {
        try {
            project.setProjectId(id);
            project = projectData.save(project);
        }
        catch (EmptyResultDataAccessException ex){
            LOG.debug("No project was found for the id" + id);
            throw new ProjectCustomException("No project was found for the id" + id);
        }
        catch (Exception ex){
            LOG.debug("Error while deleting a project" + ex);
        }
        return project;
    }

    @Override
    public boolean validateRequest(Project project){

        return isFibonacci(project.getEstimates());

    }

    private static boolean isFibonacci(int estimates) {
        int firstNumber = 0, secondNumber = 1, fibonacciNumber = 0;
        while (fibonacciNumber < estimates) {
            fibonacciNumber = firstNumber + secondNumber;
            firstNumber = secondNumber;
            secondNumber = fibonacciNumber;
        }
        if (estimates == fibonacciNumber) {
            return true;
        } else {
            return false;
        }
    }

}
