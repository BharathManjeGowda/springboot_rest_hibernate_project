package com.javainuse.service;

import com.javainuse.exception.ProjectCustomException;
import com.javainuse.model.Project;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Component
public interface ProjectService {

    List<Project> getProjectsList();

    Long saveProject(Project project);

    Project findProjectById(Long id);

    void deleteProjectById(Long id) throws ProjectCustomException;

    Project updateProject(Long id, Project project) throws ProjectCustomException;

    boolean validateRequest(Project project);

}
