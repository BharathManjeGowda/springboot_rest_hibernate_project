package com.javainuse.dao;

import com.javainuse.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {

    Project findByProjectId(Long id);

}
