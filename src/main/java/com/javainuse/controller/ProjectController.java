package com.javainuse.controller;

import java.util.List;

import com.javainuse.exception.ProjectCustomException;
import com.javainuse.model.Project;
import com.javainuse.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@Controller
public class ProjectController {

	private static final Logger LOG = LoggerFactory.getLogger(ProjectController.class);

	@Autowired
	private ProjectService projectService;

    @ResponseBody
	@RequestMapping(value = "/project", method = RequestMethod.POST)
	public Long newProject(@RequestBody Project project, HttpServletResponse response) {
		LOG.debug("Creating new Project.");
		Long projectId = null;
		boolean validate = projectService.validateRequest(project);

		if(validate)
		{
			projectId = projectService.saveProject(project);
		}
		else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		return projectId;
	}

	@ResponseBody
	@RequestMapping(value = "/project/{id}", method = RequestMethod.GET)
	public Project getProjectById(@PathVariable Long id) {
		LOG.debug("Get project for the id:" + id);
		Project project = projectService.findProjectById(id);
		return project;
	}

	@ResponseBody
	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	public List<Project> getProjectsList() {
		LOG.debug("Get all the projects");
		List<Project> allProjects = projectService.getProjectsList();
		return allProjects;
	}

	@ResponseBody
	@RequestMapping(value = "/project/{id}", method = RequestMethod.PUT)
	public Project updateProject(@PathVariable Long id ,@RequestBody Project project
			, HttpServletResponse response) throws ProjectCustomException {
		LOG.debug("Update project for the id" + id);
		Project updatedProject = null;
		boolean validate = projectService.validateRequest(project);

		if(validate)
		{
			updatedProject = projectService.updateProject(id, project);
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		return updatedProject;
	}

	@RequestMapping(value = "/project/{id}", method = RequestMethod.DELETE)
	public ResponseEntity.BodyBuilder deleteProjectById(@PathVariable Long id) throws ProjectCustomException {
		LOG.debug("Delete project for the id" + id);
		projectService.deleteProjectById(id);
		return ResponseEntity.status(HttpStatus.OK);
	}

}
