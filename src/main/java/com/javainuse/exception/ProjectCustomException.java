package com.javainuse.exception;

public class ProjectCustomException extends Exception {

    public ProjectCustomException(String message) {
        super(message);
    }

    public ProjectCustomException(String message, Throwable cause) {
        super(message, cause);
    }

}
